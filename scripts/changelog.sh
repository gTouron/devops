#!/bin/bash

# get last git tag
LAST_TAG=$(git describe --abbrev=0 --tags)

# get one tag before last tag
PREV_TAG=$(git describe --abbrev=0 --tags $LAST_TAG^)

# get commits between last and previous tag
COMMITS=$(git log $PREV_TAG..$LAST_TAG --pretty=format:"%s")

# display commits
echo "$COMMITS"
