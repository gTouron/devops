stages: # List of stages for jobs, and their order of execution
  - release
  - build
  - tests
  - quality
  - package
  - deploy
  - acceptance

variables:
  # Localisation des sources à analyser
  SONAR_SOURCE_DIR: .
  SONAR_ANALYSIS_ARGS: >-
    -Dsonar.host.url=${SONAR_HOST_URL}
    -Dsonar.organization=gtouron
    -Dsonar.projectKey=gTouron_devops
    -Dsonar.projectName=devops
    -Dsonar.projectBaseDir=${CI_PROJECT_DIR}
    -Dsonar.sources=${SONAR_SOURCE_DIR}
    -Dsonar.exclusions=${SONAR_EXCLUSIONS}
    -Dsonar.sourceEncoding=UTF-8
  BYPASS_BUILD: "false"
  ROBOT_TESTS_DIR: "robotframework/"
  ROBOT_CLI: "run-tests-in-virtual-screen"

.default_rules:
  rules:
    - if: $BYPASS_BUILD == "false"

generate_release:
 stage: release
 script:
   - LAST_TAG=$(git tag -l --sort=-v:refname | head -1)
   - PREVIOUS_TAG=$(git tag -l --sort=-v:refname | head -2 | tail -1)
   - COMMITS=$(git log $PREVIOUS_TAG..$LAST_TAG --pretty=format:"%s")
   - |
     curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $YNOV_TOKEN" \
     --data '{ "name": "New release", "tag_name": "'$CI_COMMIT_TAG'", "description": "'"$COMMITS"'" }' \
     --request POST "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases"
 only:
   - tags

composer_install:
  rules:
    - !reference [.default_rules, rules]
  stage: build
  image: solutiondrive/php-composer
  script:
    - cd src && composer install
  artifacts:
    paths:
      - src/vendor/

unit_test:
  rules:
    - !reference [.default_rules, rules]
  stage: tests
  image: solutiondrive/php-composer
  script:
    - cd src && php vendor/bin/phpunit ../tests/

sonarcloud_check:
  stage: quality
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner $SONAR_ANALYSIS_ARGS
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar" # Defines the location of the analysis task cache
    GIT_DEPTH: "0" # Tells git to fetch all the branches of the project, required by the analysis task
  rules:
    - !reference [.default_rules, rules]

build_image:
  image: docker
  services:
    - docker:dind
  stage: package
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  rules:
    - !reference [.default_rules, rules]

deploy_aws_dev:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  before_script:
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID_DEV
    - export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY_DEV
  script:
    - curl -L "https://github.com/aws/copilot-cli/releases/latest/download/copilot-linux" -o "/tmp/copilot"
    - mv /tmp/copilot /usr/local/bin/copilot
    - chmod +x /usr/local/bin/copilot
    - copilot --help
    - rm -rf /tmp/*
    - copilot svc deploy --env dev --force
  rules:
    - if: $CI_COMMIT_BRANCH == "main"

deploy_aws_prod:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  before_script:
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY='$AWS_SECRET_ACCESS_KEY'
  script:
    - curl -L "https://github.com/aws/copilot-cli/releases/latest/download/copilot-linux" -o "/tmp/copilot"
    - mv /tmp/copilot /usr/local/bin/copilot
    - chmod +x /usr/local/bin/copilot
    - copilot --help
    - rm -rf /tmp/*
    - copilot svc deploy --env prod --force
  only:
    - tags

# deploy_staging_job:
#   stage: deploy
#   image: docker:stable
#   script:
#     - apk add --no-cache python3-dev python3 openssh-client py-pip libffi-dev openssl-dev gcc libc-dev make
#     - pip install --upgrade pip
#     - pip install docker-compose
#     - export DOCKER_HOST=tcp://$PLAY_WITH_DOCKER.direct.labs.play-with-docker.com:2375
#     - docker-compose down
#     - docker-compose pull
#     - docker-compose up -d
#   environment:
#     name: staging
#     url: http://$PLAY_WITH_DOCKER.direct.labs.play-with-docker.com
#   variables:
#     PLAY_WITH_DOCKER: ip172-18-0-6-cdhto9e0qau0008f9mt0

robotframework:chrome:
  image: registry.gitlab.com/docker42/rfind:master
  stage: acceptance
  script:
    - $ROBOT_CLI $PABOT_OPT $ROBOT_OPT -v BROWSER:chrome $ROBOT_TESTS_DIR
  artifacts:
    paths:
      - output.xml
      - log.html
      - report.html
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "merge_request_event"


robotframework:firefox:
  image: registry.gitlab.com/docker42/rfind:master
  stage: acceptance
  script:
    - $ROBOT_CLI $PABOT_OPT $ROBOT_OPT -v BROWSER:firefox $ROBOT_TESTS_DIR
  artifacts:
    paths:
      - output.xml
      - log.html
      - report.html
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "merge_request_event"

