FROM liliancal/ubuntu-php-apache as dev
WORKDIR /var/www/
COPY . .
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
